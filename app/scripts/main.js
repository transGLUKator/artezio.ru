/*global GMaps:false */
/*global $:false */
/*jshint unused:false*/
(function () {
    'use strict';

    //Глобальные переменные
    var cursorInSubmenu,
        mapOptions,
        grayMapStyle,
        officeCoordinates,
        oldSrc = [], //Массив для хранения src цветных изображений блока награды
        clientsImageSrc = []; //Массив для хранения src цветных изображений блока клиенты;

    mapOptions = {
        div: '#contacts-map',
        lat: 43.299879,
        lng: -15.520816,
        zoom: 2
    };

    grayMapStyle = [
        {
            'featureType': 'water',
            'elementType': 'geometry.fill',
            'stylers': [
                {
                    'color': '#d3d3d3'
                }
            ]
        },
        {
            'featureType': 'transit',
            'stylers': [
                {
                    'color': '#808080'
                },
                {
                    'visibility': 'off'
                }
            ]
        },
        {
            'featureType': 'road.highway',
            'elementType': 'geometry.stroke',
            'stylers': [
                {
                    'visibility': 'on'
                },
                {
                    'color': '#b3b3b3'
                }
            ]
        },
        {
            'featureType': 'road.highway',
            'elementType': 'geometry.fill',
            'stylers': [
                {
                    'color': '#ffffff'
                }
            ]
        },
        {
            'featureType': 'road.local',
            'elementType': 'geometry.fill',
            'stylers': [
                {
                    'visibility': 'on'
                },
                {
                    'color': '#ffffff'
                },
                {
                    'weight': 1.8
                }
            ]
        },
        {
            'featureType': 'road.local',
            'elementType': 'geometry.stroke',
            'stylers': [
                {
                    'color': '#d7d7d7'
                }
            ]
        },
        {
            'featureType': 'poi',
            'elementType': 'geometry.fill',
            'stylers': [
                {
                    'visibility': 'on'
                },
                {
                    'color': '#ebebeb'
                }
            ]
        },
        {
            'featureType': 'administrative',
            'elementType': 'geometry',
            'stylers': [
                {
                    'color': '#a7a7a7'
                }
            ]
        },
        {
            'featureType': 'road.arterial',
            'elementType': 'geometry.fill',
            'stylers': [
                {
                    'color': '#ffffff'
                }
            ]
        },
        {
            'featureType': 'road.arterial',
            'elementType': 'geometry.fill',
            'stylers': [
                {
                    'color': '#ffffff'
                }
            ]
        },
        {
            'featureType': 'landscape',
            'elementType': 'geometry.fill',
            'stylers': [
                {
                    'visibility': 'on'
                },
                {
                    'color': '#efefef'
                }
            ]
        },
        {
            'featureType': 'road',
            'elementType': 'labels.text.fill',
            'stylers': [
                {
                    'color': '#696969'
                }
            ]
        },
        {
            'featureType': 'administrative',
            'elementType': 'labels.text.fill',
            'stylers': [
                {
                    'visibility': 'on'
                },
                {
                    'color': '#737373'
                }
            ]
        },
        {
            'featureType': 'poi',
            'elementType': 'labels.icon',
            'stylers': [
                {
                    'visibility': 'off'
                }
            ]
        },
        {
            'featureType': 'poi',
            'elementType': 'labels',
            'stylers': [
                {
                    'visibility': 'off'
                }
            ]
        },
        {
            'featureType': 'road.arterial',
            'elementType': 'geometry.stroke',
            'stylers': [
                {
                    'color': '#d6d6d6'
                }
            ]
        },
        {
            'featureType': 'road',
            'elementType': 'labels.icon',
            'stylers': [
                {
                    'visibility': 'off'
                }
            ]
        },
        {},
        {
            'featureType': 'poi',
            'elementType': 'geometry.fill',
            'stylers': [
                {
                    'color': '#dadada'
                }
            ]
        }
    ];

    officeCoordinates = [
        {
            'city': 'Moscow',
            'position': {
                'lat': 55.697035,
                'lng': 37.565346
            }
        },

        {
            'city': 'Saratov',
            'position': {
                'lat': 51.54864,
                'lng': 46.006646
            }
        },

        {
            'city': 'Nijniy Novgorod',
            'position': {
                'lat': 56.3178636,
                'lng': 44.0607548
            }
        },

        {
            'city': 'Minsk',
            'position': {
                'lat': 53.849821,
                'lng': 27.5084674
            }
        },

        {
            'city': 'Mogilev',
            'position': {
                'lat': 53.900109,
                'lng': 30.342665
            }
        },

        {
            'city': 'Vitebsk',
            'position': {
                'lat': 55.196232,
                'lng': 30.190398
            }
        },

        {
            'city': 'Harkov',
            'position': {
                'lat': 50.0396234,
                'lng': 36.2201495
            }
        }
    ];

    //Добавляем маркеры на карту
    function addMapMarkers(markers, map) {
        var index,
            arrLength;

        arrLength = markers.length;

        for (index = 0; index < arrLength; index += 1) {
            map.addMarker({
                lat: markers[index].position.lat,
                lng: markers[index].position.lng,
                title: markers[index].city
            });
        }
    }

    //Прокладываем маршрут от пользователя до офиса
    function routeUser(map, destination) {
        GMaps.geolocate({
            success: function (position) {
                map.drawRoute({
                    origin: [position.coords.latitude, position.coords.longitude],
                    destination: [destination.lat, destination.lng],
                    travelMode: 'driving',
                    strokeColor: '#131540',
                    strokeOpacity: 0.6,
                    strokeWeight: 6
                });
            },
            error: function (error) {
                window.alert('Попытка определения местоположения не удалась ' + error);
            },
            /*jshint camelcase: false */
            not_supported: function () {
                window.alert('Ваш браузер не поддерживает службу определения местоположения');
            }
        });
    }

    //Создаем экземпляр карты
    function createMap(element, lat, lng, zoom, defaultOptions) {
        var argsLength,
            map;

        argsLength = arguments.length;

        if (argsLength === 1) {
            map = new GMaps(arguments[0]);
        } else {
            map = new GMaps({
                div: arguments[0],
                lat: arguments[1],
                lng: arguments[2],
                zoom: arguments[3]
            });
        }

        return map;
    }

    //Добавляем классы для правильного отображения аффиксов при прокрутке
    function affix(top) {
        return $(window).scrollTop() > top ? $('body').addClass('affixes') : $('body').removeClass('affixes');
    }

    //Меняем цветность изображений


    function getImageSrc(img, arr) {
        arr.push(img.src);
    }

    function gray(imgObj) {
        var canvas = document.createElement('canvas'),
            canvasContext = canvas.getContext('2d'),
            imgW = imgObj.width,
            imgH = imgObj.height,
            imgPixels,
            x,
            y,
            i,
            avg;

        canvas.width = imgW;
        canvas.height = imgH;

        canvasContext.drawImage(imgObj, 0, 0);
        imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);

        for (y = 0; y < imgPixels.height; y += 1) {
            for (x = 0; x < imgPixels.width; x += 1) {
                i = (y * 4) * imgPixels.width + x * 4;
                avg = (imgPixels.data[i] + imgPixels.data[i + 1] + imgPixels.data[i + 2]) / 3;
                imgPixels.data[i] = avg;
                imgPixels.data[i + 1] = avg;
                imgPixels.data[i + 2] = avg;
            }
        }
        canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
        return canvas.toDataURL();
    }

    $(document).ready(function () {
        var contactMap; //Карта на странице контакты

        //добавляем аффиксы
        affix(123);

        $(window).on('scroll', function () {
            affix(123);
        });

        //переключаем цветность изображений
        $('.awards-list img').each(function () {
            var self = this;
            getImageSrc(self, oldSrc);
            self.src = gray(self);
        });

        $('.awards-list img').on('mouseenter', function () {
            var self = this,
                position = $(this).closest('li').index();

            self.src = oldSrc[position];
        });

        $('.awards-list img').on('mouseleave', function () {
            var self = this;
            self.src = gray(self);
        });

        $('.clients-carousel img').each(function () {
            var self = this;
            getImageSrc(self, clientsImageSrc);
            self.src = gray(self);
        });

        $('.clients-carousel img').on('mouseenter', function () {
            var self = this,
                position = $(this).closest('li').index();

            self.src = clientsImageSrc[position];
        });

        $('.clients-carousel img').on('mouseleave', function () {
            var self = this;
            self.src = gray(self);
        });

        //Слайдер клиентов
        $('.clients-carousel').jcarousel({});

        $('.jcarousel-prev').jcarouselControl({
            target: '-=1'
        });

        $('.jcarousel-next').jcarouselControl({
            target: '+=1'
        });

        //Переключаем класс .hover на пунктах меню, у которых раскрыто подменю
        $('[data-toggle=submenu]').on('mouseenter', function () {
            var element = $(this),
                submenu = $('#' + element.attr('data-sub'));

            element.addClass('hover');
            submenu.addClass('submenu_expanded');
        });

        $('[data-toggle=submenu]').on('mouseleave', function () {
            var element = $(this),
                submenu = $('#' + element.attr('data-sub'));

            setTimeout(function () {
                if (!cursorInSubmenu) {
                    element.removeClass('hover');
                }
            }, 15);

            submenu.removeClass('submenu_expanded');
        });
        
        $('.submenu').on('mouseenter', function(){
            cursorInSubmenu = true;
        });

        $('.submenu').on('mouseleave', function () {
            var element = $(this),
                menuItemId = element.attr('id'),
                menuItem = $('[data-sub=' + menuItemId + ']');

            cursorInSubmenu = false;
            element.removeClass('submenu_expanded');
            menuItem.removeClass('hover');
        });


        //Скроллим страницу вверх
        $('.back-to-top, .back-to-top-wrapper').on('click', function () {
            $('html, body').animate({
                scrollTop: 0
            }, '1000');
        });

        //Аккордион
        $('.accordion-content').find('h2').on('click', function () {
            $(this).closest('.accordion-pane').toggleClass('active');
        });

        //Карата
        contactMap = createMap(mapOptions);
        contactMap.addStyle({
            styledMapName: 'Styled Map',
            styles: grayMapStyle,
            mapTypeId: 'map_style'
        });
        contactMap.setStyle('map_style');

        addMapMarkers(officeCoordinates, contactMap); //Добавляем все маркеры на карту

        $('.directions').on('click', function () {
            var destination,
                self;

            destination = {};
            self = this;

            destination.lat = $(self).attr('data-lat');
            destination.lng = $(self).attr('data-lng');

            $('#modal').modal();

            $('#modal').on('shown.bs.modal', function () {
                var routeMap;

                routeMap = createMap('#modal-map', destination.lat, destination.lng, 13);
                routeMap.addStyle({
                    styledMapName: 'Styled Map',
                    styles: grayMapStyle,
                    mapTypeId: 'map_style'
                });
                routeMap.setStyle('map_style');

                $('.make-route').on('click', function () {
                    routeUser(routeMap, destination);
                });
            });
        });
    });
}());